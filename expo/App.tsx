import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Hotels from './src/screens/Hotels';
import Hotel from './src/screens/Hotel';

const AppNavigator = createStackNavigator({
  Hotels: {
    screen: Hotels,
  },
  Hotel: {
    screen: Hotel,
  },
});

export default createAppContainer(AppNavigator);
