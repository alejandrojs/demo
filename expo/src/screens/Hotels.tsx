import React from 'react';
import { FlatList, Alert, View } from 'react-native';
import { NavigationScreenProp, NavigationState, NavigationParams } from 'react-navigation';
import { getHotels } from '../api';
import { Hotel } from '../api/types';
import { ProgressBar } from 'react-native-paper';
import HotelItem from '../components/HotelItem';
import Search from '../components/Search';

type Props = {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
};

type State = {
  hotels: Hotel[];
  query: string;
};

export default class HotelsScreen extends React.Component<Props, State> {
  state = {
    hotels: [],
    query: '',
  };

  async componentDidMount() {
    try {
      const response = await getHotels();
      const data = await response.json();
      const { hotels, error } = data;
      if (response.ok) {
        this.setState({ hotels });
      } else {
        Alert.alert(error.code, error.message);
      }
    } catch (error) {
      Alert.alert('Error', 'Could not reach server');
    }
  }

  keyExtractor = (_item: Hotel, index: number) => String(index);

  goToHotel = (hotel: Hotel) => this.props.navigation.navigate('Hotel', { hotel });

  onChange = (query: string) => this.setState({ query });

  renderItem = ({ item }) => (
    <HotelItem hotel={item} query={this.state.query} onPress={this.goToHotel} />
  );

  render() {
    const { hotels, query } = this.state;

    return (
      <View style={{ flex: 1, padding: 10 }}>
        {hotels.length > 0 && <Search query={query} onChange={this.onChange} />}
        <FlatList
          style={{ flex: 1, marginTop: 5 }}
          data={hotels}
          initialNumToRender={10}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          ListEmptyComponent={() => <ProgressBar indeterminate />}
        />
      </View>
    );
  }
}
