import * as React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Hotel } from '../api/types';
import Stars, { YELLOW } from '../components/Stars';
import { Card, Title } from 'react-native-paper';
import MapView, { Marker } from 'react-native-maps';
import { AntDesign, MaterialCommunityIcons } from '@expo/vector-icons';
import {
  NavigationScreenProp,
  NavigationState,
  NavigationParams,
  ScrollView,
} from 'react-navigation';

type Props = {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
};

export default class HotelScreen extends React.Component<Props> {
  static navigationOptions = ({ navigation }) => {
    const hotel: Hotel = navigation.getParam('hotel');

    return {
      title: hotel.name,
      headerLeft: () => (
        <AntDesign
          style={{ marginLeft: 5 }}
          size={30}
          name="arrowleft"
          onPress={() => navigation.goBack()}
        />
      ),
    };
  };

  renderAddress = (hotel: Hotel) => (
    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
      <MaterialCommunityIcons name="map-marker" size={25} />
      <Text style={{ fontSize: 15 }}>{hotel.address}</Text>
    </View>
  );

  render() {
    const hotel: Hotel = this.props.navigation.getParam('hotel');
    const coordinate = {
      latitude: Number(hotel.latitude),
      longitude: Number(hotel.longitude),
    };

    return (
      <ScrollView style={{ flex: 1 }} contentContainerStyle={{ padding: 10 }}>
        <Card>
          <Card.Content>
            <Title>{hotel.name}</Title>
            <Stars rating={hotel.rating} />
            {this.renderAddress(hotel)}
          </Card.Content>
          <Card.Cover
            source={{
              uri: 'https://q-cf.bstatic.com/images/hotel/max1280x900/101/101430248.jpg',
            }}
          />
          <Card.Actions>
            <Text>Precio por noche</Text>
            <Text style={styles.price}>ARS {hotel.price}</Text>
          </Card.Actions>
        </Card>
        <Card style={{ marginTop: 10, marginBottom: 10 }}>
          <Card.Content>
            <MapView
              region={{
                ...coordinate,
                latitudeDelta: 5,
                longitudeDelta: 5,
              }}
              style={styles.mapStyle}
            >
              <Marker coordinate={coordinate} />
            </MapView>
          </Card.Content>
        </Card>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mapStyle: {
    width: Dimensions.get('window').width - 50,
    height: 300,
  },
  price: { marginLeft: 20, color: YELLOW, fontSize: 20 },
});
