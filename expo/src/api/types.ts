export type Hotel = {
  createdAt: string;
  address: string;
  latitude: string;
  longitude: string;
  name: string;
  price: number
  rating: number;
  updatedAt: string;
}