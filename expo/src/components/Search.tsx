import React from 'react';
import { Searchbar } from 'react-native-paper';

type Props = {
  query: string;
  onChange: (query: string) => void;
};

export default class Search extends React.Component<Props> {
  render() {
    const { query, onChange } = this.props;
    return <Searchbar placeholder="Buscar" onChangeText={onChange} value={query} />;
  }
}
