import React from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View, StyleSheet } from 'react-native';

type Props = {
  rating: number;
};

export const YELLOW = '#f9d71c';

const Stars = React.memo<Props>(
  (props: Props) => {
    return (
      <View style={styles.starsContainer}>
        {Array.from({ length: Number(props.rating) }, (v, i) => (
          <FontAwesome key={String(i)} color={YELLOW} size={25} name="star" style={styles.star} />
        ))}
      </View>
    );
  },
  () => true
);

const styles = StyleSheet.create({
  starsContainer: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  star: {
    marginLeft: 2,
  },
});

export default Stars;
