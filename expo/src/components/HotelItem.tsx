import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Hotel } from '../api/types';
import { Card, Title } from 'react-native-paper';
import Stars from './Stars';

type Props = {
  onPress: (hotel: Hotel) => void;
  hotel: Hotel;
  query: string;
};

const YELLOW = '#f9d71c';

const HotelItem = React.memo<Props>(({ hotel, query, onPress }) => {
  if (!hotel.name.toLowerCase().includes(query.toLowerCase())) {
    return null;
  }

  return (
    <Card style={styles.card} onPress={() => onPress(hotel)}>
      <Card.Cover
        source={{ uri: 'https://q-cf.bstatic.com/images/hotel/max1280x900/101/101430248.jpg' }}
      />
      <Card.Actions>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 0.7 }}>
            <Title>{hotel.name}</Title>

            <Stars rating={hotel.rating} />
          </View>
          <View style={styles.priceContainer}>
            <Text style={styles.priceTitle}>Precio por noche</Text>
            <Text style={styles.price}>ARS {hotel.price}</Text>
          </View>
        </View>
      </Card.Actions>
    </Card>
  );
});

const styles = StyleSheet.create({
  card: {
    marginBottom: 10,
  },
  priceContainer: {
    flex: 0.3,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  priceTitle: {
    fontSize: 12,
    fontWeight: '300',
  },
  price: {
    color: YELLOW,
    fontSize: 20,
  },
});

export default HotelItem;
