## Instalar dependencias

Usando npm:

```bash
npm i
```

Usando yarn

```bash
yarn
```

## Levantar app

Cambiar IP de maquina que levante el server en `src/api/index.ts`

luego correr proyecto Expo

```bash
npm start
```

se puede instalar apretando la letra `a` si adb esta configurado y el celular conectado
o sino se puede instalar la app de Expo desde Play Store o AppStore y escanear el codigo QR.
