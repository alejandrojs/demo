import { Hotel } from 'entities';
import { createEntity } from 'utils/typeorm';
import faker from 'faker';

const seedHotels = (): Promise<Hotel[]> => {
  const hotels = [];

  for (let index = 0; index < 100; index += 1) {
    hotels.push(
      createEntity(Hotel, {
        name: faker.random.words(3),
        address: faker.address.streetAddress(true),
        latitude: faker.address.latitude(),
        longitude: faker.address.longitude(),
        price: faker.random.number({ min: 1000, max: 5000 }),
        rating: faker.random.number({ min: 1, max: 5 }),
      }),
    );
  }

  return Promise.all(hotels);
};

const seedDatabase = async (): Promise<boolean> => {
  try {
    await seedHotels();

    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export default seedDatabase;
