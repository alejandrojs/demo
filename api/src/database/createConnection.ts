import { createConnection, Connection } from 'typeorm';

import { Hotel } from 'entities';

const createDatabaseConnection = (): Promise<Connection> =>
  createConnection({
    type: 'mongodb',
    url: process.env.DB_URL,
    database: process.env.DB_DATABASE,
    replicaSet: process.env.DB_REPLICA,
    entities: [Hotel],
    authSource: 'admin',
    useUnifiedTopology: true,
    synchronize: true,
  });

export default createDatabaseConnection;
