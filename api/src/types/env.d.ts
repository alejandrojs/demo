declare namespace NodeJS {
  export interface ProcessEnv {
    DB_URL: string;
    DB_REPLICA: string;
    DB_DATABASE: string;
  }
}
