import {
  BaseEntity,
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ObjectIdColumn,
} from 'typeorm';

@Entity()
class Hotel extends BaseEntity {
  @ObjectIdColumn()
  id: number;

  @Column('text')
  name: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @Column('string')
  address: string;

  @Column('string')
  latitude: string;

  @Column('string')
  longitude: string;

  @Column('integer')
  rating: number;

  @Column('number')
  price: number;
}

export default Hotel;
