import * as hotels from 'controllers/hotels';

export const attachRoutes = (app: any): void => {
  app.get('/hotels', hotels.list);
};
