import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';

import { Hotel } from 'entities';
import { EntityNotFoundError } from 'errors';

type EntityConstructor = typeof Hotel;

export const createEntity = async <T extends EntityConstructor>(
  Constructor: T,
  input: Partial<InstanceType<T>>,
): Promise<InstanceType<T>> => {
  const instance = Constructor.create(input);
  return instance.save() as InstanceType<T>;
};

export const listEntities = async <T extends EntityConstructor>(
  Constructor: T,
  options?: FindManyOptions,
): Promise<Array<InstanceType<T>>> => {
  const instance = await Constructor.find(options);
  if (!instance) {
    throw new EntityNotFoundError(Constructor.name);
  }

  return instance;
};
