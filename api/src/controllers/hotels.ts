import { Hotel } from 'entities';
import { catchErrors } from 'errors';
import { listEntities } from 'utils/typeorm';

export const list = catchErrors(async (_req, res) => {
  const hotels = await listEntities(Hotel);
  res.respond({ hotels });
});
